import { client } from "../src/connection";
import { BookDAO } from "../src/daos/book-dao"
import { BookDaoPostgres } from "../src/daos/book-dao-postgres";
import { Book } from "../src/entities"

const bookDAO:BookDAO = new BookDaoPostgres();

test("Create a book", async ()=>{
    // any entity that has not been saved somewhere 
    // should have an ID of 0, standard convention
    const testBook:Book = new Book(0,'The Hobbit','JRR',true,1,0);
    const result:Book = await bookDAO.createBook(testBook);
    // saved entity should have a non-zero id
    expect(result.bookId).not.toBe(0);
});

// integration test, requires 2 or more functions pass
test("Get book by ID", async ()=>{
    let book:Book = new Book(0,'Dracula','Stoker',true,1,0);
    book = await bookDAO.createBook(book);
    // avoid hard coded ID values, difficult to maintain
    let retrievedBook:Book = await bookDAO.getBookById(book.bookId);
    expect(retrievedBook.title).toBe(book.title);
});

test("Get all books", async ()=>{
    let book1:Book = new Book(0,'Sapiens','Yuval',true,0,0);
    let book2:Book = new Book(0,'1984','Orwell',true,1,1);
    let book3:Book = new Book(0,'Paradox','Barry',true,0,1);
    await bookDAO.createBook(book1);
    await bookDAO.createBook(book2);
    await bookDAO.createBook(book3);
    const books:Book[]= await bookDAO.getAllBooks();
    expect(books.length).toBeGreaterThanOrEqual(3);
});

test("Update book", async ()=>{
    let book:Book = new Book(0,'Castle','Jackson',true,1,0);
    book = await bookDAO.createBook(book);
    // to update, efit it and pass it into a method
    book.quality = 4
    book = await bookDAO.updateBook(book);
    expect(book.quality).toBe(4);
});

test("Delete book", async ()=>{
    let book:Book = new Book(0,'Frank','Mary',true,1,0);
    book = await bookDAO.createBook(book);
    const result:boolean = await bookDAO.deleteBookById(book.bookId);
    expect(result).toBeTruthy();
});
// ends connection after tests are finished
afterAll(async()=>{
    client.end();
})

// try to avoid methods in program that return void
// methods that don't return data are difficult to test
// for most part, want methods to take in objects