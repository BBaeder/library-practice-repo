// an entity is a class that stores info
// this info will ultimately be persisted somewhere
// usually very little logic contained within
// they SHOULD ALWAYS have one field that is a primary key, ID

export class Book{
    constructor(
        public bookId:number,
        public title:string,
        public author:string,
        public isAvail:boolean,
        public quality:number,
        // dates stores as unix epoch time
        // seconds from midnight January 1970
        public returnDate:number
    ){}
}