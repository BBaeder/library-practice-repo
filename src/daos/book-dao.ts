// DAO (Data Access Object)
// class responsible for persisting an entity
// DAO should support CRUD operations
import { Book } from "../entities";
export interface BookDAO{
    // CREATE
    createBook(book:Book):Promise<Book>;
    // READ
    getAllBooks():Promise<Book[]>;
    getBookById(bookId:number):Promise<Book>;
    // UPDATE
    updateBook(book:Book):Promise<Book>;
    // DELETE
    deleteBookById(bookID:number):Promise<boolean>;
}