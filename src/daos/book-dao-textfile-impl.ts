import { Book } from "../entities";
import { BookDAO } from "./book-dao";
import { readFile, writeFile } from "fs/promises";
import { MissingResourceError } from "../errors";

export class BookDaoTextFile implements BookDAO {
    async createBook(book: Book): Promise<Book> {
        const fileData:Buffer = await readFile('C:\\Users\\baede\\Documents\\Revature\\LibraryAPI\\books.txt')
        const textData:string = fileData.toString(); // turn file into string
        const books:Book[] = JSON.parse(textData); // take JSON text and turn into object
        book.bookId = Math.round(Math.random()*1000); // random number ID for book
        books.push(book); // add book to the array
        await writeFile('C:\\Users\\baede\\Documents\\Revature\\LibraryAPI\\books.txt',JSON.stringify(books));
        return book;
    }
    async getAllBooks(): Promise<Book[]> {
        const fileData:Buffer = await readFile('C:\\Users\\baede\\Documents\\Revature\\LibraryAPI\\books.txt')
        const textData:string = fileData.toString();
        const books:Book[]=JSON.parse(textData);
        return books;
    }
    async getBookById(bookId: number): Promise<Book> {
        const fileData:Buffer = await readFile('C:\\Users\\baede\\Documents\\Revature\\LibraryAPI\\books.txt')
        const textData:string = fileData.toString();
        const books:Book[]=JSON.parse(textData);
        for(const b of books){
            if(b.bookId === bookId){
                return b; 
            }
        }
        throw new MissingResourceError(`Book with id ${bookId} could not be located.`);
    }
    async updateBook(book: Book): Promise<Book> {
        const fileData:Buffer = await readFile('C:\\Users\\baede\\Documents\\Revature\\LibraryAPI\\books.txt')
        const textData:string = fileData.toString();
        const books:Book[] = JSON.parse(textData);
        for(let i = 0; i < books.length; i++){
            if(books[i].bookId === book.bookId){
                books[i] = book;
            }   
        }
        await writeFile('C:\\Users\\baede\\Documents\\Revature\\LibraryAPI\\books.txt',JSON.stringify(books));
        return book;
    }
    async deleteBookById(bookID: number): Promise<boolean> {
        const fileData:Buffer = await readFile('C:\\Users\\baede\\Documents\\Revature\\LibraryAPI\\books.txt')
        const textData:string = fileData.toString();
        const books:Book[] = JSON.parse(textData);
        for(let i = 0; i < books.length; i++){
             if(books[i].bookId === bookID){
                 books.splice(i);
                 await writeFile('C:\\Users\\baede\\Documents\\Revature\\LibraryAPI\\books.txt',JSON.stringify(books));
                 return true;
             }
        }
        return false;
    }
}