import express from 'express';
import { Book } from './entities';
import { MissingResourceError } from './errors';
import BookService from './services/book-service';
import { BookServiceImpl } from './services/book-service-impl';

const app = express();
app.use(express.json()); // Middleware

// our application routes should use the services we created
// to do the heavey lifting, minimize logic in routes that is 
// not related directly to HTTP requests and responses
const bookService:BookService = new BookServiceImpl();

app.get("/books", async (req, res)=>{
        const books:Book[] = await bookService.retrieveAllBooks(); 
        res.send(books);
})
app.get("/books/:id", async (req, res) => {
    try {
        const bookId = Number(req.params.id);
        const book: Book = await bookService.retrieveBookByID(bookId);
        res.send(book);
    } catch (error) {
        if (error instanceof MissingResourceError) {
            res.status(404);
            res.send(error);
        }
    }
});
app.post("/books", async (req, res)=>{
    let book:Book = req.body;
    book = await bookService.registerBook(book);
    res.send(book);
})
app.listen(3000,()=>{console.log("Application Started")});