import { BookDAO } from "../daos/book-dao";
import { BookDaoPostgres } from "../daos/book-dao-postgres";
import { Book } from "../entities";
import BookService from "./book-service";

export class BookServiceImpl implements BookService{
    
    bookDAO:BookDAO = new BookDaoPostgres();
    
    registerBook(book: Book): Promise<Book> {
        return this.bookDAO.createBook(book);
    }
    retrieveAllBooks(): Promise<Book[]> {
        return this.bookDAO.getAllBooks();
    }
    retrieveBookByID(bookId: number): Promise<Book> {
        return this.bookDAO.getBookById(bookId);
    }
    async checkoutBookByID(bookId: number): Promise<Book> {
        let book:Book = await this.bookDAO.getBookById(bookId);
        book.isAvail = false;
        book.returnDate = Date.now()+1_209_600
        book = await this.bookDAO.updateBook(book);
        return book;
    }
    async checkinBookByID(bookId: number): Promise<Book> {
        let book:Book = await this.bookDAO.getBookById(bookId);
        book.isAvail = true;
        book.returnDate = 0;
        book = await this.bookDAO.updateBook(book);
        return book;
    }
    SearchByTitle(title: string): Promise<Book[]> {
        throw new Error("Method not implemented.");
    }
    modifyBook(book: Book): Promise<Book> {
        throw new Error("Method not implemented.");
    }
    removeBookById(bookId: number): Promise<boolean> {
        throw new Error("Method not implemented.");
    }
    
}