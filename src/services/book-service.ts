import { Book } from "../entities";

// service interface contains all methods that  RESTful
// web service will find helpful
// include 2 types of methods:
// 1) Methods that perform CRUD operations
// 2) Business Logic operations
export default interface BookService{
    registerBook(book:Book):Promise<Book>;
    retrieveAllBooks():Promise<Book[]>;
    retrieveBookByID(bookId:number):Promise<Book>;
    checkoutBookByID(bookId:number):Promise<Book>;
    checkinBookByID(bookId:number):Promise<Book>;
    SearchByTitle(title:string):Promise<Book[]>;
    modifyBook(book:Book):Promise<Book>;
    removeBookById(bookId:number):Promise<boolean>;
}