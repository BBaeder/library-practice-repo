import { Client } from 'pg';

export const client = new Client({
    user:'postgres',
// SHOULD NEVER STORE PASSWORDS IN CODE!!!!!!!
// search environment in windows and add a new
// environmental variable, then do as follows
    password:process.env.DBPASSWORD,
    database:process.env.DBNAME,
    port:5432,
    host:'34.150.195.205'
})
client.connect();